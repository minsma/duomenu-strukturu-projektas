# MultiSet projektas #

1. Išnagrinėjamas mūsų svetainėje pateiktas projektas, kuriame demonstruojamas minimalios DS kūrimas pagal apsibrėžtą interfeisą. Pavyzdyje pateiktas steko klasės dviejų realizacijos variantų projektas.  
2. Pasirenkamas paskaitos metu nagrinėtas ar literatūroje aprašytas linijinės duomenų struktūros ADT. Esant skirtingų realizacijų galimybei, konkretizuojamas realizacijos pobūdis.  
Galimų duomenų struktūrų sąrašas (tikslesni aprašai su paieška "data structures" + vardas):  
•    Stack - realizuota pavyzdyje  
•    Queue- maksimalus balas 5  
•    Deque- maksimalus balas 5  
•    Double linked list - max 7  •    Priority queue (8)  
•    Bit array  (8)          •    Dynamic array (8)      •    Circular buffer  
•    Sparse array     •    Sparse matrix     
•    Set - įvairūs varijantai     •    Multiset  
•    Unrolled linked list         •    VList         •    Skip list  
Rikiavimo metodų realizacija nurodoma atskirai ir priklauso nuo struktūros pobūdžio.