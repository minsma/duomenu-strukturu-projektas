package projektas;

import java.util.Objects;

public class Vartotojas {
    String vartotojoVardas;
    String saliesPavadinimas;

    public Vartotojas(String vartotojoVardas, String saliesPavadinimas) {
        this.vartotojoVardas = vartotojoVardas;
        this.saliesPavadinimas = saliesPavadinimas;
    }

    public String getVartotojoVardas() {
        return vartotojoVardas;
    }

    public void setVartotojoVardas(String vartotojoVardas) {
        this.vartotojoVardas = vartotojoVardas;
    }

    public String getSaliesPavadinimas() {
        return saliesPavadinimas;
    }

    public void setSaliesPavadinimas(String saliesPavadinimas) {
        this.saliesPavadinimas = saliesPavadinimas;
    }

    @Override
    public String toString() {
        return saliesPavadinimas;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.saliesPavadinimas);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vartotojas other = (Vartotojas) obj;
        if (!Objects.equals(this.saliesPavadinimas, other.saliesPavadinimas)) {
            return false;
        }
        return true;
    }
    
    
}
