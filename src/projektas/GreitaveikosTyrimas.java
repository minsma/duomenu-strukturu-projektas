package projektas;

import java.util.ArrayList;
import java.util.HashSet;

public class GreitaveikosTyrimas {
    
    Multiset<Integer> aibe1 = new Multiset<Integer>();
    HashSet<Integer> aibe2 = new HashSet<Integer>();
    
    int[] tiriamiKiekiai = {5000, 6000, 10000};
    
    void pirmasTyrimas(int elementuKiekis){
        Long t1 = System.nanoTime();
        
        for(int i = 0; i < elementuKiekis; i++){
            aibe1.add(i);
        }
        
        Long t2 = System.nanoTime();
        
        for(int i = 0; i < elementuKiekis; i++){
            aibe2.add(i);
        }
        
        Long t3 = System.nanoTime();
        
        Ks.ouf("%7d %7.4f %7.4f \n", elementuKiekis,
                (t2-t1)/1e9, (t3-t2)/1e9);
    }
    
    void antrasTyrimas(int elementuKiekis){        
        ArrayList<Integer> list = new ArrayList<Integer>();
        
        for(int i = elementuKiekis/2; i > 0; i--)
            list.add(i);
        
        long t1 = System.nanoTime();
        
        for(int i : list){
            aibe1.contains(i);
        }
        
        //aibe1.containsAll(list);
        
        long t2 = System.nanoTime();
        
        for(int i : list){
            aibe2.contains(i);
        }
        
        //aibe2.containsAll(list);
        
        long t3 = System.nanoTime();
        
        Ks.ouf("%7d %7.4f %7.4f \n", elementuKiekis,
                (t2-t1)/1e9, (t3-t2)/1e9);
    }
    
    
    
    public void tyrimuVykdymas(){
        Ks.oun("Kiekis  MSetAdd  HSetAdd");
        for(int kiekis : tiriamiKiekiai)
            pirmasTyrimas(kiekis);
        
        Ks.oun("Kiekis  MSetCAll  HSetCAll");
        for(int kiekis : tiriamiKiekiai)
            antrasTyrimas(kiekis);
    }
    
    public static void main(String[] args){
        long memTotal = Runtime.getRuntime().totalMemory();
        Ks.oun("memTotal= "+memTotal);
        new GreitaveikosTyrimas().tyrimuVykdymas();
   }   
}
