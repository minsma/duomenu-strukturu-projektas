package projektas;

import java.util.Collection;
import java.util.Set;

public interface MultisetADT<E> {
    
    /**
     * Funkcija pridedanti i Multiset elementa, count kartu.
     * @param element - pridedamas elementas.
     * @param count - kiek kartu pridedamas elementas i Multiset.
     * @return grazinamas, pries dejima buves elemento kiekis.
     */
    int add(E element, int count);
    
    /**
     * Funkcija idedanti nurodyta elementa i Multiset.
     * @param element - idedamas elementas.
     * @return true, visada.
     */
    boolean add(E element);
    
    /**
     * Prideda visus elementus is nurodytos kolekcijos i Multiset.
     * @param c - Kolekcija, kurios elementai bus prideti i Multiset.
     * @return true, jeigu visi kolekcijos elementai buvo prideti sekmingai.
     */
    boolean addAll(Collection<? extends E> c);
    
    /**
     * Prideda visus elementus is nurodyto masyvo i Multiset.
     * @param arg masyvas, saugantis pridedamas reiksmes.
     */
    void addAll(E... arg);

    /**
     * Pasalina viena pasikartojima nurodyto elemento is Multiset. 
     * Jeigu elementas kartojasi tik karta, tai pasalinamas pats elementas.
     * @param element elementas, kurio pasikartojimas bus panaikintas
     * @return true, jeigu pasikartojimas buvo rastas ir pasalintas
     */
    boolean remove(Object element);
    
    /**
     * Pasalina nurodyta elemento kieki count kartu.
     * @param element salinamas elementas
     * @param count kelis kartus bus salinamas elementas
     * @return grazinamas pries salinima buves elementu kiekis
     */
    int remove(Object element, int count);
    
    /**
     * Patikrinama ar siame Multiset yra bent vienas elemento pasikartojimas.
     * @param element elementas, kurio pasikartojimo ieskomaa Multiset.
     * @return true, jeigu bent viena karta kartojasi elementas, 
     * kitu atveju false.
     */
    boolean contains(Object element);
    
    /**
     * Tikrinama ar Multiset turi bent karta pasikartojancius
     * visus nurodytos kolekcijos elementus.
     * @param c kolekcija, kurios elementus tikrinama ar turi Multiset.
     * @return true, jeigu bent karta kartojasi visi nurodytos 
     * kolekcijos elementai
     */
    boolean containsAll(Collection<?> c);
    
    /**
     * Atnaujina elemento pasikartojima i nurodyta count arba prideda nauja 
     * elementa, jeigu jo dar nebuvo pries tai su nurodytu count pasikartojimu
     * @param element elementas, kurio pasikartojima atnaujina.
     * @param count kiekis i kuri atnaujinama
     * @return Multiset elementu kiekis pries atnaujinima. 
     */
    int setCount(E element, int count);
    
    /**
     * Multiset esancio elemento kiekio grazinimas
     * @param element elementas, kuris turi buti Multiset.
     * @return 0, jeigu nera tokio elemento, kitu atveju elemento 
     * pasikartojimo kiekis.
     */
    int count(Object element);
    
    /**
     * Sudaromas nepasikartojanciu eleentu aibes vaizdas, siame Multiset.
     * @return grazinamas vaizdas aibes nepasikartojanciu elementu 
     * siame multiset.
     */
//    Set<E> elementSet();
    
    /**
     * Tikrinama ar Multiset yra tuscias.
     * @return false, jeigu yra daugiau nei 0 elementu, true jeigu tuscias.
     */
    boolean isEmpty();
    
    /**
     * Elementu kiekis Multiset.
     * @return visu elementu su ju pasikartojimais kiekis.
     */
    int size();
    
}
