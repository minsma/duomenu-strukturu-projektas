package projektas;

import java.util.Arrays;

public class Test {
    
    public static void main(String[] args) {
        Multiset<String> multiset = new Multiset();
        
        // add metodu bandymai
        System.out.println("Add metodu bandymai:");
        multiset.add("JAV");
        multiset.add("Lietuva", 2);
        multiset.add("Lietuva");
        multiset.addAll("Peru", "Vokietija", "Latvija");
        multiset.addAll(Arrays.asList("Estija", "Bolivija", "Irakas", "Iranas"));
        
        System.out.println(multiset);
        
        System.out.println("Remove metodu bandymai:");
        // remove metodu bandymai
        multiset.remove("Lietuva", 2);
        multiset.remove("JAV");
        
        System.out.println(multiset);
        
        System.out.println("setCount metodo bandymai:");
        // setCount metodo bandymai
        multiset.setCount("JAV", 8);
        multiset.setCount("Lietuva", 1);
        multiset.setCount("Brazilija", 2);
        
        System.out.println(multiset);
    }
    
}
