package projektas;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;

public class Multiset<E> implements MultisetADT<E>{
    
    // Nesikartojanciu reiksmiu sarasas. 
    //private List<E> values;
    
    // Sveikuju skaiciu masyvas, kuriame saugoma kiek,
    // kuri reiksme pasikartoja kartu.
    //private List<Integer> recurrence;
    
    List<Element> values;
    
    // Klaidos pranesimas, tikrinant ar Multiset kiekis yra maziau nei 0
    private final String ERROR_MSG = "Kiekis negali buti neigiamas: ";
    
    /**
     * Konstruktorius, sukuriantis reiksmiu ir pasikartojimu 
     * ArrayList tipo sarasas.
     */
    public Multiset(){
        values = new ArrayList<Element>();
        //recurrence = new ArrayList<>();
    }
    
    /**
     * Funkcija pridedanti i Multiset elementa, count kartu.
     * @param element - pridedamas elementas.
     * @param count - kiek kartu pridedamas elementas i Multiset.
     * @return grazinamas, pries dejima buves elemento kiekis.
     */
    @Override
    public int add(E element, int count){
        if(count < 0){
            throw new IllegalArgumentException(ERROR_MSG + count);
        }
        
        Element e = new Element(element, count);
        
        int index = values.indexOf(e);
        int prevCount = 0;
        
        if(index != -1){
            //prevCount = recurrence.get(index);
            prevCount = values.get(index).recurrence;
            //recurrence.set(index, prevCount + count);
            values.get(index).setRecurrence(prevCount + count);
        } else if(count != 0){
            Element elment = new Element(element, count);
            values.add(elment);
            //values.add(element);
            //recurrence.add(count);
        }
        
        return prevCount;
    }
    
    /**
     * Funkcija idedanti nurodyta elementa i Multiset.
     * @param element - idedamas elementas.
     * @return true, visada.
     */
    @Override
    public boolean add(E element){
        return add(element, 1) >= 0;
    }
    
    /**
     * Prideda visus elementus is nurodytos kolekcijos i Multiset.
     * @param c - Kolekcija, kurios elementai bus prideti i Multiset.
     * @return true, jeigu visi kolekcijos elementai buvo prideti sekmingai.
     */
    @Override
    public boolean addAll(Collection<? extends E> c){
        for(E element : c){
            add(element, 1);
        }
        
        return true;
    }
    
    
    /**
     * Prideda visus elementus is nurodyto masyvo i Multiset.
     * @param arg masyvas, saugantis pridedamas reiksmes.
     */
    @Override
    public void addAll(E... arg){
        for(E element : arg){
            add(element, 1);
        }
    }
        
    /**
     * Pasalina viena pasikartojima nurodyto elemento is Multiset. 
     * Jeigu elementas kartojasi tik karta, tai pasalinamas pats elementas.
     * @param element elementas, kurio pasikartojimas bus panaikintas
     * @return true, jeigu pasikartojimas buvo rastas ir pasalintas
     */
    @Override
    public boolean remove(Object element){
        return remove(element, 1) > 0;
    }
    
    /**
     * Pasalina nurodyta elemento kieki count kartu.
     * @param element salinamas elementas
     * @param count kelis kartus bus salinamas elementas
     * @return grazinamas pries salinima buves elementu kiekis
     */
    @Override
    public int remove(Object element, int count){
        if(count < 0){
            throw new IllegalArgumentException(ERROR_MSG + count);
        }
        
        Element elemnt = new Element(element, count);
        
        int index = values.indexOf(elemnt);
        
        if(index == -1){
            return 0;
        }
        
        //int prevCount = recurrence.get(index);
        int prevCount = values.get(index).recurrence;
        
        if(prevCount > count){
            //recurrence.set(index, prevCount - count);
            values.get(index).setRecurrence(prevCount - count);
        } else{
            values.remove(index);
            //recurrence.remove(index);
            values.get(index).setRecurrence(values.get(index).getRecurrence() - 1);
        }
        
        return prevCount;
    }
    
    /**
     * Patikrinama ar siame Multiset yra bent vienas elemento pasikartojimas.
     * @param element elementas, kurio pasikartojimo ieskomaa Multiset.
     * @return true, jeigu bent viena karta kartojasi elementas, 
     * kitu atveju false.
     */
    @Override
    public boolean contains(Object element){        
        return values.contains(element);
    }
    
    /**
     * Tikrinama ar Multiset turi bent karta pasikartojancius
     * visus nurodytos kolekcijos elementus.
     * @param c kolekcija, kurios elementus tikrinama ar turi Multiset.
     * @return true, jeigu bent karta kartojasi visi nurodytos 
     * kolekcijos elementai
     */
    @Override
    public boolean containsAll(Collection<?> c){
        return values.containsAll(c);
    }
    
    /**
     * Atnaujina elemento pasikartojima i nurodyta count arba prideda nauja 
     * elementa, jeigu jo dar nebuvo pries tai su nurodytu count pasikartojimu
     * @param element elementas, kurio pasikartojima atnaujina.
     * @param count kiekis i kuri atnaujinama
     * @return Multiset elementu kiekis pries atnaujinima. 
     */
    @Override
    public int setCount(E element, int count){
        if(count < 0){
            throw new IllegalArgumentException(ERROR_MSG + count);
        }
        
        if(count == 0){
            remove(element);
        }
        
        Element elm = new Element(element, count);
        
        int index = values.indexOf(elm);
        
        if(index == -1){
            return add(element, count);
        }
        
        //int prevCount = recurrence.get(index);
        int prevCount = values.get(index).recurrence;
        
        //recurrence.set(index, count);
        values.get(index).setRecurrence(count);
        
        return prevCount;
    }
    
    /**
     * Multiset esancio elemento kiekio grazinimas
     * @param element elementas, kuris turi buti Multiset.
     * @return 0, jeigu nera tokio elemento, kitu atveju elemento 
     * pasikartojimo kiekis.
     */
    @Override
    public int count(Object element){
       
        int index = values.indexOf(element);
        
        return (index == -1) ? 0 : values.get(index).recurrence;
    
        //recurrence.get(index);
    }
    
    /**
     * Sudaromas nepasikartojanciu eleentu aibes vaizdas, siame Multiset.
     * @return grazinamas vaizdas aibes nepasikartojanciu elementu 
     * siame multiset.
     */
//    @Override
//    public Set<E> elementSet(){
//        return values.stream().collect(Collectors.toSet());
//    }
    
    /**
     * Tikrinama ar Multiset yra tuscias.
     * @return false, jeigu yra daugiau nei 0 elementu, true jeigu tuscias.
     */
    @Override
    public boolean isEmpty(){
        return values.size() == 0;
    }
    
    /**
     * Elementu kiekis Multiset.
     * @return visu elementu su ju pasikartojimais kiekis.
     */
    @Override
    public int size(){
        int size = 0;
        
        for(Element i : values){
            size += i.getRecurrence();
        }
        
        return size;
    }
    
    /**
     * @return eilute spausdinimui.
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder("");
        
        for(int i = 0; i < values.size(); i++){
            sb.append(values.get(i).value);
            
//            if(recurrence.get(i) > 1){
//                sb.append(" x ").append(recurrence.get(i));
//            }
            
            if(values.get(i).recurrence > 1){
                sb.append(" x ").append(values.get(i).recurrence);
            }
            
            if(i != values.size() - 1){
                sb.append(", ");
            }
            
            sb.append("\n");
        }
        
        return sb.toString();
    }
    
    /**
     * @return Grazinamas unikalios reiksmes
     */
    public ArrayList<String> getValues() {
        
        ArrayList<String> list = new ArrayList();
        
        for(Element el : values){
            list.add(el.getValue() + " x " + el.getRecurrence());
        }
        
        return list;
    }
}