package projektas;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class DemonstracinisPanaudojimas {
    
    public static Multiset skaitytiIsFailo(){
        Multiset vartotojai = new Multiset();
        
        try{
            FileInputStream fstream = new FileInputStream("duomenys/data.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

            String strLine;

            while ((strLine = br.readLine()) != null)   {
                String[] parts = strLine.split("\t");
                vartotojai.add(new Vartotojas(parts[0], parts[1]));
            }
        
            br.close();
        } catch (FileNotFoundException e) {
            Ks.ern("Duomenų failas nerastas");
            System.exit(0);
        } catch (IOException e) {
            Ks.ern("Failo skaitymo klaida");
            System.exit(0);
        }
        
        
        return vartotojai;
    }
    
    public static void isvestiUnikalesSalis(Multiset vartotojai){
        ArrayList<String> sarasas = vartotojai.getValues();
        
        for(String var : sarasas){
            Ks.oun(var);
        }        
    }
    
    public static void isvestiIFailaUnikalesSalis(Multiset vartotojai){
        ArrayList<String> sarasas = vartotojai.getValues();
        
        FileWriter writer = null;
        
        try {
            writer = new FileWriter("duomenys/unikaliosSalys.txt");
            
            for(String var : sarasas){
                writer.write(var + "\n");
            }   
            
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        Multiset vartotojai = skaitytiIsFailo();
        
        isvestiUnikalesSalis(vartotojai);
        
        isvestiIFailaUnikalesSalis(vartotojai);
    }
}
