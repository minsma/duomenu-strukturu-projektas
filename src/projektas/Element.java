package projektas;

import java.util.Objects;

public class Element<E> {
    E value;
    int recurrence;

    public Element(E value, int recurrence) {
        this.value = value;
        this.recurrence = recurrence;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }

    public int getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(int recurrence) {
        this.recurrence = recurrence;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.value);
        hash = 31 * hash + this.recurrence;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Element<?> other = (Element<?>) obj;
        
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }
    
       
}
